using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace Multiply.Tests
{
    [ExcludeFromCodeCoverage]
    public sealed class FinalTests
    {
        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        [InlineData(0, 0)]
        [InlineData(0, -1)]
        [InlineData(-1, 0)]
        public void Zero_is_the_zero_property(int first, int second)
        {
            // Arrange
            var sut = CreateSystemUnderTest();
            
            // Act
            var result = sut.Multiply(first, second);

            // Assert
            result.Should().Be(0);
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        [InlineData(2, 1, 2)]
        [InlineData(1, -2, -2)]
        [InlineData(-2, 1, -2)]
        public void One_is_the_identity_element(int first, int second, int expected)
        {
            // Arrange
            var sut = CreateSystemUnderTest();

            // Act
            var result = sut.Multiply(first, second);

            // Assert
            result.Should().Be(expected);
        }

        [Theory]
        [InlineData(2, 3, 6)]
        [InlineData(3, 2, 6)]
        [InlineData(-2, 3, -6)]
        [InlineData(3, -2, -6)]
        public void Multiplication_is_commutative(int first, int second, int expected)
        {
            // Arrange
            var sut = CreateSystemUnderTest();

            // Act
            var result = sut.Multiply(first, second);

            // Assert
            result.Should().Be(expected);
        }

        [Theory]
        [InlineData(1, -1)]
        [InlineData(-1, 1)]
        [InlineData(2, -2)]
        [InlineData(-2, 2)]
        public void Minus_one_produces_the_additive_inverse_of_the_other_factor(int second, int expected)
        {
            // Arrange
            var sut = CreateSystemUnderTest();
            
            // Act
            var result = sut.Multiply(-1, second);

            // Assert
            result.Should().Be(expected);
        }
        
        private static IKata CreateSystemUnderTest() => new Kata();
    }
}