﻿namespace Multiply
{
    public interface IKata
    {
        /// <summary>
        /// Multiplies two integer
        /// </summary>
        /// <param name="first">First factor</param>
        /// <param name="second">Second factor</param>
        /// <returns>The product of the multiplication</returns>
        ///
        /// <example>
        /// first: 4
        /// second: 2
        /// product: 8
        /// </example>
        int Multiply(int first, int second);
    }
}